//FILE TO CONTROL TWO SETS OF LEDS
#include <stdlib.h>
#include <stdio.h> // Used for printf() statements
#include <wiringPi.h> // Include WiringPi library!
# define FILENAME "LED1.dat"
#define FILENAME2 "LED2.dat"
// Pin number declarations. We're using the Broadcom chip pin numbers.

const int ledPin = 17; // Regular LED - Broadcom pin 23, P1 pin 16
const int ledPin2 = 22;
int main(void)
{
// Setup stuff:
wiringPiSetupGpio(); // Initialize wiringPi -- using Broadcom pin numbers
pinMode(ledPin, OUTPUT); // Set regular LED as output
pinMode(ledPin2, OUTPUT);
int ledon; //input bit that determines if light is on
int led2on;
FILE *input_file;
FILE *input_file2;
input_file= fopen(FILENAME, "r");
if(input_file == NULL)
{
        printf("error opening file\n");
        exit(1);
}
input_file2= fopen(FILENAME2, "r");
if(input_file2 == NULL)
{
        printf("error opening file2\n");
        exit(1);
}
printf("C GPIO program running! Press CTRL+C to quit.\n");

while(1)
{
        digitalWrite(ledPin, LOW);
        digitalWrite(ledPin2, LOW);
        fscanf(input_file, "%d", &ledon);
        fscanf(input_file2, "%d", &led2on);
        while(1){
                if (ledon){
                digitalWrite(ledPin, HIGH);}
                if(led2on){
                digitalWrite(ledPin2, HIGH);}
        }
}
fclose(input_file);
fclose(input_file2);
return 0;
}
