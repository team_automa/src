#CPE 185 driver file 
#Please note the current HTML file is being hosted on 172.20.10.7

import RPi.GPIO as GPIO
import serial
import time
import urllib2



#Display Code
import Adafruit_DHT
from time import sleep
import os
import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
#PI pin config for Display
RST = 24
#note for SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0
disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST, i2c_address=0x3C)
# Initialize library.
disp.begin()
# Clear display.
disp.clear()
disp.display()
# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))
# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)
# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)
# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = 2
shape_width = 20
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = padding
# Load default font.
font = ImageFont.load_default()
font18 = ImageFont.truetype('Minecraftia.ttf', 18)
font20 = ImageFont.truetype('Minecraftia.ttf', 20)
font24 = ImageFont.truetype('Minecraftia.ttf', 24)
font12 = ImageFont.truetype('Minecraftia.ttf', 12)



#serial used to communicate with app
ser = serial.Serial('/dev/ttyAMA0',9600)


#servo controls
control= [5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]

#pins for components
led1= 27
led2= 22
fanPin= 17
servo= 25

#set up for I/O
GPIO.setup(servo, GPIO.OUT)
GPIO.setup(led1, GPIO.OUT)
GPIO.setup(led2, GPIO.OUT)
GPIO.setup(fanPin, GPIO.OUT)

#Initial states
GPIO.output(led1, GPIO.LOW)
GPIO.output(led2, GPIO.LOW)
GPIO.output(fanPin, GPIO.LOW)
p= GPIO.PWM(servo, 50)

q= GPIO.PWM(fanPin, 270)
 
Lights1 = 0
Lights2 = 0
Fan = 0
door = 0
Temp = 1
try:
	while True:
		L1 = urllib2.urlopen("http://172.20.10.7/LEDstate.txt").read();
		L2 = urllib2.urlopen("http://172.20.10.7/LEDstate2.txt").read();
		D1 = urllib2.urlopen("http://172.20.10.7/doorState.txt").read();
		F1 = urllib2.urlopen("http://172.20.10.7/fanState.txt").read();
		T1 = urllib2.urlopen("http://172.20.10.7/tempState.txt").read();
		#turn on display
		if int(T1) == 1 and Temp ==0:
			print("TEMP ON")
			Temp = 1
			#TURN ON TEMP
			# Sensor should be set to Adafruit_DHT.DHT11,
			# Adafruit_DHT.DHT22, or Adafruit_DHT.AM2302.
			sensor = Adafruit_DHT.DHT11
			pin = 4
			humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
			if humidity is not None and temperature is not None:
				sleep(1)
				str_temp = ' {0:0.2f} *C '.format(temperature) 	
				str_hum  = ' {0:0.2f} %'.format(humidity)
				print('Temp={0:0.1f}*C Humidity={1:0.1f}%'.format(temperature, humidity))	
				draw.rectangle((0,0,width,height), outline=0, fill=0)
				#disp.clear()
				#disp.display()		
				draw.text((3, top),    'Temperature/Humidity',  font=font, fill=255)
				draw.text((x, top+16), str_temp, font=font18, fill=255)
				draw.text((x, top+36), str_hum, font=font18, fill=255)
				disp.image(image)
				disp.display()	
		elif int(T1) == 0 and Temp == 1:
			print("Display Name")
			Temp = 0
			#TURN ON DISPLAY
			disp.clear()
			disp.display()
			draw.rectangle((0,0,width,height), outline=0, fill=0)
			draw.text((x, top),    'Project',  font=font, fill=255)
			draw.text((x, top+18), 'Homestead', font=font18, fill=255)
			# Display image.
			disp.image(image)
			disp.display()
			
		#turn on lights 1
		if int(L1) == 1 and Lights1 == 0:
			print("LIGHTS ON")
			Lights1 = 1
			GPIO.output(led1, GPIO.HIGH)
		#turn off lights 1
		elif int(L1) == 0 and Lights1 == 1:
			print("LIGHTS OFF")
			Lights1 = 0
			GPIO.output(led1, GPIO.LOW)
		#turn on lights 2
		if int(L2) == 1 and Lights2 == 0:
			print("LIGHTS2 ON")
			Lights2 = 1
			GPIO.output(led2, GPIO.HIGH)
		#turn off lights 2
		elif int(L2) == 0 and Lights2 == 1:
			print("LIGHTS2 OFF")
			Lights2 = 0
			GPIO.output(led2, GPIO.LOW) 
		#open lock
		if int(D1) == 1 and door == 0:
			
			p.start(2.5)
			print("OPEN LOCK")
			door = 1
			for x in range(9, 0, -1):  #returns servo motor to 0 degrees(unlocked)
	      			p.ChangeDutyCycle(control[x])
	        		time.sleep(0.04)
	
		#close lock
		elif int(D1) == 0 and door == 1:
			print("CLOSED LOCK")
			door = 0
			for x in range(11): #turns servo motor to 90 degrees(locked)
	        		p.ChangeDutyCycle(control[x])
	        		time.sleep(0.04)
	
		#turn on fan
		if int(F1) == 1 and Fan == 0:
			q.start(0)
			print("FAN ON")
			Fan = 1
			q.ChangeDutyCycle(80)
		#turn off fan
		elif int(F1) == 0 and Fan == 1:
			print("FAN OFF")
			Fan = 0
			q.stop()

except KeyboardInterrupt:
	GPIO.cleanup()

