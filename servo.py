import RPi.GPIO as GPIO
import time
control= [5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]

servo= 22 #GPIO pin for servo motor
flock = 0 #initialized data for lock variable
GPIO.setmode(GPIO.BOARD)
GPIO.setup(servo, GPIO.OUT)
p= GPIO.PWM(servo, 50)
p.start(2.5)

f= open('LOCK.dat',"r") #open and read integer in lock.dat file
for line in f:
        flock= int(line.strip('\n'))

if flock==1:
        for x in range(11): #turns servo motor to 90 degrees(locked)
                p.ChangeDutyCycle(control[x])
                time.sleep(0.04)
        else:
                time.sleep(1)   
else:
        for x in range(9, 0, -1):  #returns servo motor to 0 degrees(un$
                p.ChangeDutyCycle(control[x])
                time.sleep(0.04)
        else:
                time.sleep(1)

GPIO.cleanup() #clears out the gpio

